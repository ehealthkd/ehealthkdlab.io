import json


def build_table(collection, scenario):
    with open("docs/json/%s.json" % collection) as fp:
        data = json.load(fp)

    rows = []

    for team, submits in data.items():
        for submit in submits:
            scenario_data = submit[scenario]

            f1 = str(round(scenario_data["f1"], 3))
            precision = str(round(scenario_data["precision"], 3))
            recall = str(round(scenario_data["recall"], 3))

            rows.append((team, submit["submit"], f1, precision, recall))

    rows.sort(key=lambda x: x[2], reverse=True)

    with open("docs/gen/%s-%s.md" % (collection, scenario), "w") as fp:
        fp.write("| **Team** | **Submission** | **F1** | **Precision** | **Recall** |\n")
        fp.write("|----------|----------------|--------|---------------|------------|\n")

        for row in rows:
            fp.write("| ")
            fp.write(" | ".join(row))
            fp.write(" |\n")


def main():
    for col in ["dev", "test"]:
        for scn in range(1, 4):
            build_table(col, "scenario%i" % scn)


if __name__ == "__main__":
    main()
