# Official leaderboard

This page contains official results for each scenario, both in the **test** and the **development** collection.
Check the [evaluation page](evaluation) to learn how to get your results published here automatically.

## Test Collection

These are the three leaderboards for the scenarios of the **test** collection.

### Scenario 1

{!docs/gen/test-scenario1.md!}

### Scenario 2

{!docs/gen/test-scenario2.md!}

### Scenario 3

{!docs/gen/test-scenario3.md!}

## Development Collection

These are the three leaderboards for the scenarios of the **development** collection.

### Scenario 1

{!docs/gen/dev-scenario1.md!}

### Scenario 2

{!docs/gen/dev-scenario2.md!}

### Scenario 3

{!docs/gen/dev-scenario3.md!}
