<script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML'></script>

# Subtask B: Relation extraction

Subtask B continues from the output of Subtask A, by linking the entities detected and labelled in the input document. The purpose of this subtask is to recognize all relevant semantic relationships between the entities recognized. Eight of the thirteen semantic relations defined for this challenge can be identified in the following example:

![](img/task_b.png)

The semantic relations are divided in different categories:

**General relations (6):** general-purpose relations between two concepts (it involves Concept, Action, Predicate, and Reference) that have a specific semantic. When any of these relations applies, it is preferred over a domain relation --tagging a key phrase as a link between two information units--, since their
semantic is independent of any textual label:

* **is-a:** indicates that one entity is a subtype, instance, or member of the class identified by the other.
* **same-as:** indicates that two entities are semantically the same.
* **has-property:** indicates that one entity has a given property or characteristic.
* **part-of:** indicates that an entity is a constituent part of another.
* **causes:** indicates that one entity provoques the existence or occurrence of another.
* **entails:** indicates that the existence of one entity implies the existence or occurrence of another.

**Contextual relations (3):** allow to refine an entity (it involves **Concept**, **Action**, **Predicate**, and **Reference**) by attaching modifiers. These are:

* **in-time:** to indicate that something exists, occurs or is confined to a time-frame, such as in "*exposición*" in-time "*verano*".
* **in-place:** to indicate that something exists, occurs or is confined to a place or location.
* **in-context:** to indicate a general context in which something happens, like a mode, manner, or state, such as "*exposición*" in-context "*prolongada*".

**Action roles (2):** indicate which role play the entities related to an **Action**:

* **subject:** indicates who performs the action, such as in "*[el] asma afecta [...]*".
* **target:** indicates who receives the effect of the action, such as in "*[...] afecta [las] vías respiratorias*".
Actions can have several subjects and targets, in which case the semantic interpreted is that the union of the subjects performs the action over each of the targets.

**Predicate roles (2):** indicate which role play the entities related to a **Predicate**:

* **domain:** indicates the main entity on which the predicate applies.
* **arg:** indicates an additional entity that specifies a value for the predicate to make sense. The exact semantic of this argument depends on the semantic of the predicate label, such as in "*mayores [de] 60 años*", where the predicate label "*mayores*" indicates that "*60 años*" is a quantity, that restricts the minimum age for the predicate to be true.
